"use strict";

const DATA = [
	{
		"first name": "Олексій",
		"last name": "Петров",
		photo: "./img/trainers/trainer-m1.jpg",
		specialization: "Басейн",
		category: "майстер",
		experience: "8 років",
		description:
			"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
	},
	{
		"first name": "Марина",
		"last name": "Іванова",
		photo: "./img/trainers/trainer-f1.png",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
	},
	{
		"first name": "Ігор",
		"last name": "Сидоренко",
		photo: "./img/trainers/trainer-m2.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
	},
	{
		"first name": "Тетяна",
		"last name": "Мороз",
		photo: "./img/trainers/trainer-f2.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "10 років",
		description:
			"Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
	},
	{
		"first name": "Сергій",
		"last name": "Коваленко",
		photo: "./img/trainers/trainer-m3.jpg",
		specialization: "Тренажерний зал",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
	},
	{
		"first name": "Олена",
		"last name": "Лисенко",
		photo: "./img/trainers/trainer-f3.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "4 роки",
		description:
			"Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
	},
	{
		"first name": "Андрій",
		"last name": "Волков",
		photo: "./img/trainers/trainer-m4.jpg",
		specialization: "Бійцівський клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
	},
	{
		"first name": "Наталія",
		"last name": "Романенко",
		photo: "./img/trainers/trainer-f4.jpg",
		specialization: "Дитячий клуб",
		category: "спеціаліст",
		experience: "3 роки",
		description:
			"Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
	},
	{
		"first name": "Віталій",
		"last name": "Козлов",
		photo: "./img/trainers/trainer-m5.jpg",
		specialization: "Тренажерний зал",
		category: "майстер",
		experience: "10 років",
		description:
			"Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
	},
	{
		"first name": "Юлія",
		"last name": "Кравченко",
		photo: "./img/trainers/trainer-f5.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "4 роки",
		description:
			"Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
	},
	{
		"first name": "Олег",
		"last name": "Мельник",
		photo: "./img/trainers/trainer-m6.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "12 років",
		description:
			"Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
	},
	{
		"first name": "Лідія",
		"last name": "Попова",
		photo: "./img/trainers/trainer-f6.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
	},
	{
		"first name": "Роман",
		"last name": "Семенов",
		photo: "./img/trainers/trainer-m7.jpg",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
	},
	{
		"first name": "Анастасія",
		"last name": "Гончарова",
		photo: "./img/trainers/trainer-f7.jpg",
		specialization: "Басейн",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
	},
	{
		"first name": "Валентин",
		"last name": "Ткаченко",
		photo: "./img/trainers/trainer-m8.jpg",
		specialization: "Бійцівський клуб",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
	},
	{
		"first name": "Лариса",
		"last name": "Петренко",
		photo: "./img/trainers/trainer-f8.jpg",
		specialization: "Дитячий клуб",
		category: "майстер",
		experience: "7 років",
		description:
			"Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
	},
	{
		"first name": "Олексій",
		"last name": "Петров",
		photo: "./img/trainers/trainer-m9.jpg",
		specialization: "Басейн",
		category: "майстер",
		experience: "11 років",
		description:
			"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
	},
	{
		"first name": "Марина",
		"last name": "Іванова",
		photo: "./img/trainers/trainer-f9.jpg",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
	},
	{
		"first name": "Ігор",
		"last name": "Сидоренко",
		photo: "./img/trainers/trainer-m10.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
	},
	{
		"first name": "Наталія",
		"last name": "Бондаренко",
		photo: "./img/trainers/trainer-f10.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "8 років",
		description:
			"Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
	},
	{
		"first name": "Андрій",
		"last name": "Семенов",
		photo: "./img/trainers/trainer-m11.jpg",
		specialization: "Тренажерний зал",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
	},
	{
		"first name": "Софія",
		"last name": "Мельник",
		photo: "./img/trainers/trainer-f11.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "6 років",
		description:
			"Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
	},
	{
		"first name": "Дмитро",
		"last name": "Ковальчук",
		photo: "./img/trainers/trainer-m12.png",
		specialization: "Дитячий клуб",
		category: "майстер",
		experience: "10 років",
		description:
			"Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
	},
	{
		"first name": "Олена",
		"last name": "Ткаченко",
		photo: "./img/trainers/trainer-f12.jpg",
		specialization: "Бійцівський клуб",
		category: "спеціаліст",
		experience: "5 років",
		description:
			"Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
	},
];

let sortDATA = [...DATA]

const templateTrainers = document.getElementById('trainer-card')
const trainerSection = document.querySelector('.trainers-cards__container')
const sorting = document.querySelector('.sorting')
const sidebar = document.querySelector('.sidebar')
const loader = document.querySelector('.content')
const sortButtons = document.querySelectorAll('.sorting__btn')
const ulTrainers = document.querySelector('.trainers-cards__container')
const modalTemplate = document.querySelector('#modal-template')
let source = DATA
const formFilter = document.querySelector('.sidebar__filters')
const allDirect = document.querySelector('#all-direction')
const gym = document.querySelector('#gym')
const fightClub = document.querySelector('#fight-club')
const kidClub = document.querySelector('#kids-club')
const pool = document.querySelector('#swimming-pool')
const allCategory = document.querySelector('#all-category')
const master = document.querySelector('#master')
const specialist = document.querySelector('#specialist')
const instructor = document.querySelector('#instructor')
let sortStorage = localStorage.getItem('sort')
let specStorage = localStorage.getItem('specialization')
let catStorage = localStorage.getItem('category')
let specStorageText = localStorage.getItem('specializationText')
let catStorageText = localStorage.getItem('categoryText')
let j = 0

sortButtons.forEach(button => button.setAttribute('id' , j++))
const originalData = [...DATA];


let activeButton = document.querySelector('.sorting__btn--active')
activeButton.classList.remove('sorting__btn--active')
if(sortStorage == 'default' || sortStorage == null){
	document.getElementById("0").classList.add('sorting__btn--active')
}
else if(sortStorage == 'lastName'){
	document.getElementById("1").classList.add('sorting__btn--active')
	sortDATA.sort((a, b) => a['last name'].localeCompare(b['last name'], 'uk'))
}
else if(sortStorage == 'experience'){
	document.getElementById("2").classList.add('sorting__btn--active')
	sortDATA.sort((a, b) => parseInt(a.experience) < parseInt(b.experience) ? 1 : -1)
}
if(specStorage == 'all'){
	allDirect.checked = true
}
else if(specStorage){
	sortDATA = sortDATA.filter(value => value.specialization == specStorageText)
	document.querySelector(specStorage).checked = true
}
if(catStorage == 'all'){
	allCategory.checked = true
}
else if(catStorage){
	sortDATA = sortDATA.filter(value => value.category == catStorageText)
	document.querySelector(catStorage).checked = true
}

function trainerCardsShow(x){
	loader.style.display = "flex"
	ulTrainers.innerHTML = ""
	setTimeout(()=>{
		let i = 0
		
		for(let elem of x){
			let trainerCards = document.importNode(templateTrainers.content , true);
			trainerCards.querySelector('.trainer__name').innerHTML = elem['first name'] + " " + elem['last name']
			trainerCards.querySelector('.trainer__img').src = elem.photo
			elem.id = i++
			trainerCards.querySelector('.trainer').setAttribute('id' , elem.id)
			trainerSection.append(trainerCards)
		}
		source = x
		loader.style.display = 'none'
	} , 2000)
	
}

trainerCardsShow(sortDATA)

sorting.removeAttribute('hidden')
sidebar.removeAttribute('hidden')

// MODAL


sortButtons.forEach((button)=>{
	button.addEventListener('click' , (event)=>{
		let delClass = document.querySelector('.sorting__btn--active')
		if(delClass){delClass.classList.remove('sorting__btn--active')}
		button.classList.add('sorting__btn--active')
			if(event.target.getAttribute('id') == 0){
			trainerCardsShow(DATA)
			allCategory.checked = true
			allDirect.checked = true
			localStorage.setItem('sort' , 'default')
		}
		if(event.target.getAttribute('id') == 1){
			sortDATA.sort((a, b) => a['last name'].localeCompare(b['last name'], 'uk'))
			trainerCardsShow(sortDATA)
			localStorage.setItem('sort' , 'lastName')
			if(document.querySelector(specStorage)){
				document.querySelector(specStorage).checked = true
			}
			if(document.querySelector(catStorage)){
				document.querySelector(catStorage).checked = true
			}
		}
		if(event.target.getAttribute('id') == 2){
			sortDATA.sort((a, b) => parseInt(a.experience) < parseInt(b.experience) ? 1 : -1)
			trainerCardsShow(sortDATA)
			localStorage.setItem('sort' , 'experience')
			if(document.querySelector(specStorage)){
				document.querySelector(specStorage).checked = true
			}
			if(document.querySelector(catStorage)){
				document.querySelector(catStorage).checked = true
			}
		}
	})
})


// SORTING

document.addEventListener('click', (event) => {
    if (event.target.classList.contains('trainer__show-more')) {
        let i = event.target.parentElement.getAttribute('id')
        let modalWindow = document.importNode(modalTemplate.content, true)
        modalWindow.querySelector('.modal__img').src = source[i].photo
        modalWindow.querySelector('.modal__name').innerHTML = source[i]["first name"] + " " + source[i]["last name"]
        modalWindow.querySelector('.modal__point--category').innerHTML = "Категорія: " + source[i].category
        modalWindow.querySelector('.modal__point--experience').innerHTML = "Досвід: " + source[i].experience
        modalWindow.querySelector('.modal__point--specialization').innerHTML = "Напрям тренера: " + source[i].specialization
        modalWindow.querySelector('.modal__text').innerHTML = source[i].description
        document.body.append(modalWindow)
        document.body.style.overflow = "hidden"

        const closeModal = document.querySelector('.modal__close')
        closeModal.addEventListener('click', () => {
            document.querySelector('.modal').remove()
            document.body.style.overflow = 'auto'
        })

        document.querySelector('.modal').addEventListener('click', (event) => {
            if (document.querySelector('.modal__body') && !document.querySelector('.modal__body').contains(event.target)) {
                document.querySelector('.modal').remove()
                document.body.style.overflow = 'auto'
            }
        })
    }
});

// FILTERING

function filterTrainers(){
formFilter.addEventListener('submit', (event) => {
    event.preventDefault();
	let sortButton = document.querySelector('.sorting__btn--active')
	if(sortButton.getAttribute('id') == 1){
		originalData.sort((a, b) => a['last name'].localeCompare(b['last name'], 'uk'))
	}
	else if(sortButton.getAttribute('id') == 2){
		originalData.sort((a, b) => parseInt(a.experience) < parseInt(b.experience) ? 1 : -1)
	}
    let filteredDATA = [...originalData];
    if (allDirect.checked) {
        filteredDATA = [...originalData];
		localStorage.setItem('specialization' , 'all')
    } else if (gym.checked) {
        filteredDATA = filteredDATA.filter((value) => value.specialization == "Тренажерний зал");
		localStorage.setItem('specialization' , '#gym')
		localStorage.setItem('specializationText' , 'Тренажерний зал')
    } else if (fightClub.checked) {
        filteredDATA = filteredDATA.filter((value) => value.specialization == "Бійцівський клуб");
		localStorage.setItem('specialization' , '#fight-club')
		localStorage.setItem('specializationText' , 'Бійцівський клуб')
    } else if (kidClub.checked) {
        filteredDATA = filteredDATA.filter((value) => value.specialization == "Дитячий клуб");
		localStorage.setItem('specialization' , '#kids-club')
		localStorage.setItem('specializationText' , 'Дитячий клуб')
    } else if (pool.checked) {
        filteredDATA = filteredDATA.filter((value) => value.specialization == "Басейн");
		localStorage.setItem('specialization' , '#swimming-pool')
		localStorage.setItem('specializationText' , 'Басейн')
    }
    let catDATA = [...filteredDATA];
    if (allCategory.checked) {
        catDATA = [...filteredDATA];
		localStorage.setItem('category' , 'all')
    } else if (master.checked) {
        catDATA = catDATA.filter((value) => value.category == 'майстер');
		localStorage.setItem('category' , '#master')
		localStorage.setItem('categoryText' , 'майстер')
    } else if (specialist.checked) {
        catDATA = catDATA.filter((value) => value.category == 'спеціаліст');
		localStorage.setItem('categoryText' , 'спеціаліст')
		localStorage.setItem('category' , '#specialist')
    } else if (instructor.checked) {
        catDATA = catDATA.filter((value) => value.category == 'інструктор');
		localStorage.setItem('categoryText' , 'інструктор')
		localStorage.setItem('category' , '#instructor')
    }
    sortDATA = [...catDATA];
    trainerCardsShow(sortDATA);
});
}
filterTrainers()
